﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using LLBML.Messages;
using LLBML.States;
using LLBML.Utils;
using Multiplayer;
using LLHandlers;
using UnityEngine;
using LLScreen;

namespace GameChat
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfo.PLUGIN_ID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        private static ChatBehavior chatBehavior;
        public const int chatMessageId = 300;
        public const int chatHandshakeId = 301;
        public static Plugin instance;
        private static UTF8Encoding utf8Encoding;

        public ConfigEntry<KeyCode> chatKey;
        public ConfigEntry<int> offsetX, offsetY;
        public ConfigEntry<bool> sendHandshake;

        private string[] lastLobbyState;
        private string[] lastLobbyStateHandshakeReceived;
        public static bool IS_GAME => GameStates.GetCurrent() == GameState.GAME && JOMBNFKIHIC.EAENFOJNNGP != OnlineMode.NONE;
        public static bool IS_LOBBY => GameStates.GetCurrent() == GameState.LOBBY_ONLINE;
        public static bool IS_GAME_PAUSED => OGONAGCFDPK.instance.DFACBJGBGGA != null;
        public static bool ChatOpen => chatBehavior.chatWindowOpen;
        public static float ChatLastClosed => chatBehavior.lastClose;

        void Awake()
        {
            Logger.LogInfo("Enabling game chat plugin");
            GameObject obj = new GameObject("GameChatPlugin");
            DontDestroyOnLoad(obj);
            chatBehavior = obj.AddComponent<ChatBehavior>();

            utf8Encoding = new UTF8Encoding(true);
            //InvokeRepeating("TestChatbox", 1.0f, 1.0f);

            var harmony = new Harmony(PluginInfo.PLUGIN_ID);
            harmony.PatchAll();

            MessageApi.RegisterCustomMessage(this.Info, chatMessageId, "chatMessage", receivedChatMessage);
            MessageApi.RegisterCustomMessage(this.Info, chatHandshakeId, "chatHandshake", receivedChatHandshake);
            InitConfig();

            lastLobbyState = new string[4];
            lastLobbyStateHandshakeReceived = new string[4];
            InvokeRepeating("CheckLobbyState", 5.0f, 5.0f);
            instance = this;
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);
        }

        void CheckLobbyState()
        {
            if (!sendHandshake.Value) return;
            if (IS_GAME || IS_LOBBY)
            {
                string[] lobbyState = GetLobbyState();
                if (!IsEqualLobbyState(lobbyState, lastLobbyState))
                {
                    SendHandshake();
                    lastLobbyState = lobbyState;
                }
            }
        }

        private string[] GetLobbyState()
        {
            string[] outS = new string[4];
            for (int i = 0; i < 4; i++)
            {//
                ALDOKEMAOMB pl = ALDOKEMAOMB.LGGEDLHOFIO[i];
                if (pl != null)
                {
                    if (pl.KLEEADMGHNE == null)
                    {
                        outS[i] = "peerNull";
                    }
                    else
                    {
                        outS[i] = pl.KLEEADMGHNE.peerId;
                    }
                }
                else
                {
                    outS[i] = "None";
                }
            }
            return outS;
        }

        private bool IsEqualLobbyState(string[] state1, string[] state2)
        {
            for (int i = 0; i < 4; i++)
            {
                if (state1[i] != state2[i]) return false;
            }

            return true;
        }

        private void SendHandshake()
        {
            if (IS_GAME || IS_LOBBY)
            {
                P2P.SendOthers(new Message((Msg)Plugin.chatHandshakeId, P2P.localPeer.playerNr, -1, null, -1));
            }
            else
            {
                Logger.LogInfo("Tried to handshake to nobody, abandoned");
            }
            
        }

        public void SendChat(string text)
        {
            if (IS_GAME || IS_LOBBY)
            {
                P2P.SendOthers(new Message((Msg)Plugin.chatMessageId, P2P.localPeer.playerNr, -1, (object)String2Bytes(text), -1));
            }
            else
            {
                Logger.LogInfo("Sent local chat to nobody");
            }
        }
            

        private static byte[] String2Bytes(string output)
        {
            return utf8Encoding.GetBytes(output);
        }

        void InitConfig()
        {
            chatKey = Config.Bind<KeyCode>("Toggles", "chatKey", KeyCode.Return, "Key to open the chatbox while in-game");
            //chatLobbyKey = Config.Bind<KeyCode>("Toggles", "chatLobbyKey", KeyCode.Return, "Key to open the chatbox while in lobby");
            sendHandshake = Config.Bind<bool>("Toggles", "chatHandshake", true, "Tell other clients you have Game Chat installed (recommended). You will still receive other client's handshakes.");
            offsetX = Config.Bind<int>("Tuning", "offsetX", 1300, new ConfigDescription("X Position of chat box", new AcceptableValueRange<int>(0, 1500)));
            offsetY = Config.Bind<int>("Tuning", "offsetY", -180, new ConfigDescription("Y Position of chat box", new AcceptableValueRange<int>(-300, 650)));
        }


        public static void receivedChatMessage(Message msg)
        {
            ALDOKEMAOMB pl = ALDOKEMAOMB.LGGEDLHOFIO[msg.playerNr];
            string plName = pl.KLEEADMGHNE.peerName;
            byte[] message = (byte[])msg.ob ;

            chatBehavior.CreateChat(new ChatBehavior.ChatMessage(plName, System.Text.Encoding.UTF8.GetString(message)));
        }

        public static void receivedChatHandshake(Message msg)
        {
            string[] lobbyState = instance.GetLobbyState();
            if (!instance.IsEqualLobbyState(instance.lastLobbyStateHandshakeReceived, lobbyState))
            {
                ALDOKEMAOMB pl = ALDOKEMAOMB.LGGEDLHOFIO[msg.playerNr];
                string plName = pl.KLEEADMGHNE.peerName;

                chatBehavior.CreateSystemChat(String.Format("Player {0} has Game Chat! Press <{1}> to use gamechat in-game.", plName, instance.chatKey.Value.ToString()));
                instance.lastLobbyStateHandshakeReceived = lobbyState;
            }
            
        }

        
    }


    [HarmonyPatch(typeof(InputHandler), nameof(InputHandler.GetInput))]
    public static class InputPatch
    {
        public static bool Prefix(ALDOKEMAOMB player,ref  bool __result)
        {
            if (!player.GAFCIHKIGNM) return true;  //if not local, continue
            if (Plugin.ChatOpen)
            {
                __result = false;
                return false; ;
            }
            return true;
        }
    }

    [HarmonyPatch(typeof(ScreenPlayers), nameof(ScreenPlayers.DoUpdate))]
    public static class ScreenPlayersPatch
    {
        public static bool Prefix()
        {
            if (Plugin.ChatOpen || Time.time - Plugin.ChatLastClosed < 0.3f)
            {
                return false; // return early, block inputs (namely Esc) to menu
            }
            return true;
        }
    }

}
